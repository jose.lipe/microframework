<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 05/11/17
 * Time: 23:09
 */
namespace Jose\Renderer;

interface PHPRendererInterface
{
    public function setData($data);
    public function run();
}