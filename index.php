<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 05/11/17
 * Time: 20:47
 */
require __DIR__.'/vendor/autoload.php';

$app = new Jose\App;
$app->setRenderer(new \Jose\Renderer\PHPRenderer());

require __DIR__ . '/src/router.php';

$app->run();
